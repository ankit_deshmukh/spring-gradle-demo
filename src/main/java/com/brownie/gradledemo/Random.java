package com.brownie.gradledemo;

public class Random {

    public static void main(String[] args) {

        int arr[] = {1, 5, 8, 9, 10, 17, 17, 20};
        int size = arr.length;

        System.out.println(maxRod(arr, size));
    }

    static int maxRod(int[] price, int n)
    {
        int[] val = new int[n+1];
        val[0]=0;

        for(int i=1; i<=n; i++)
        {
            int maxVal = Integer.MIN_VALUE;

            for(int j=0; j<i; j++)
            {
                maxVal = Math.max(maxVal, price[j] + val[i-1-j]);
            }

            val[i] = maxVal;

        }

        return val[n];
    }

}
